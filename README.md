# thesis-template

This template is the result of collecting a lot of resources about how to structure and write a thesis. 
The result is far from being the perfect template, but it should be a good starting point to simplify the tedious work of layout a thesis.

## Template structure
The template is composed by several files. 

The idea is that each part of the thesis has a dedicated TeX file (introduction.tex, abstract.tex, ...) that you can edit independently. 

Each chapter has its own folder to better organize the external resources that you may include (images, pdfs, ...)

`thesis-main.tex` is used to organize all the parts into a single document. 

## What you have to modify in this template
This template does not have your personal information.

In addition to the thesis's body you must modify the title-page document with all your personal information and rename the chapters, removing the ones that you do not use.

## Presentation
Under the folder `presentation_slides/` it is possible to find the template for the thesis defence (both 16/9 and 4/3 format).

## References
The layout of the thesis follows the [guidelines](http://www.tedoc.polimi.it/uploads/media/PoliTesi_Istruzioni.pdf) indicated by Polimi's library.

Guidelines for Abstract, Introduction and Conclusions are from a [template](http://airwiki.ws.dei.polimi.it/images/3/3c/SchemaTesi.tgz) written by Matteo Matteucci.

Inside the `documentation/` folder it is possible to find some usefull book about LaTeX.

## Useful tools
Here you can find usefull tools for the creation of equations, tables and figures:

*	https://mathpix.com/
*	http://www.tablesgenerator.com/
* 	https://www.mathcha.io/
*	http://latexdraw.sourceforge.net/